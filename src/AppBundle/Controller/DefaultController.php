<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="home")
     */
    public function indexAction()
    {
        return $this->render('AppBundle::home.html.twig');
    }

    /**
     * @Route("/loginTemplate", name="login")
     */
    public function LoginAction()
    {
        return $this->render('AppBundle:user:login.html.twig');
    }

    /**
     * @Route("/registerTemplate", name="registration")
     */
    public function RegistrationAction()
    {
        return $this->render('AppBundle:user:registration.html.twig');
    }

    /**
     * @Route("/invoice/template", name="invoice_template")
     */
    public function UserAddAction()
    {
        return $this->render('AppBundle:invoice:template.html.twig');
    }
}
