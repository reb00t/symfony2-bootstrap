<?php

namespace AppBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

class Builder extends ContainerAware
{
    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav navbar-nav');

        $menu->addChild('Invoicing', array('label' => 'Invoicing'))
            ->setAttribute('dropdown', true)
            ->setAttribute('icon', 'fa fa-file-text');

        $menu['Invoicing']->addChild('Invoice Template', array('route' => 'invoice_template'))
            ->setAttribute('icon', 'fa fa-plus');

        return $menu;
    }

    public function userMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav navbar-nav navbar-right');
        $securityContext = $this->container->get('security.context');
        if ($securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED') || $securityContext->isGranted('IS_AUTHENTICATED_FULLY')) {
            // if im logged in
            $username = $securityContext->getToken()->getUser()->getUsername();
            $menu->addChild('User', array('label' => ucfirst(strtolower($username))))
                ->setAttribute('dropdown', true)
                ->setAttribute('icon', 'fa fa-user');
            $menu['User']->addChild('Logout', array('route' => 'fos_user_security_logout'))
                ->setAttribute('icon', 'fa fa-edit');
        }else{
            $menu->addChild('Login', array('route' => 'fos_user_security_login'))
                ->setAttribute('icon', 'fa fa-login');
            $menu->addChild('Register', array('route' => 'fos_user_registration_register'))
                ->setAttribute('icon', 'fa fa-register');
        }
        return $menu;
    }
}