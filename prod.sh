#!/bin/bash

php composer.phar dump-autoload --optimize && rm -Rf app/cache app/logs && mkdir app/cache app/logs && chmod -Rf 777 app/cache app/logs && php app/console assetic:dump --env=prod && php app/console assets:install web --env=prod
